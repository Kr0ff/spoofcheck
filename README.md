# spoofcheck

### **This tool has been updated to support python 3 !**

> Credits go to the original creator of the script @BishopFox (https://github.com/BishopFox/spoofcheck)

A program that checks if a domain can be spoofed from. The program checks SPF and DMARC records for weak configurations that allow spoofing. 

Additionally it will alert if the domain has DMARC configuration that sends mail or HTTP requests on failed SPF/DKIM emails.

Usage:
```bash
./spoofcheck.py -t [DOMAIN]
```

Domains are spoofable if any of the following conditions are met:
- Lack of an SPF or DMARC record
- SPF record never specifies `~all` or `-all`
- DMARC policy is set to `p=none` or is nonexistent


## Dependencies
- dnspython
- colorama
- tldextract
- <strike>emailprotectionslib</strike> - No longer required as it is now located in `libs` directory

## Setup

Run `pip install -r requirements.txt` from the command line to install the required dependencies.

## Coming Soon - Maybe..
- Standalone Windows executable
- Basic GUI option
- Tests

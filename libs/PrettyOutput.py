from colorama import Fore, Style
from colorama import init as color_init

color_init()

def output_good(line):
    print(Fore.GREEN + Style.BRIGHT + "[+]" + Style.RESET_ALL, line)

def output_indifferent(line):
    print(Fore.MAGENTA + Style.BRIGHT + "[&]" + Style.RESET_ALL, line)

def output_error(line):
    print(Fore.YELLOW + Style.BRIGHT + "[!]" + Style.NORMAL, line, Style.BRIGHT, Style.RESET_ALL)

def output_bad(line):
    print(Fore.RED + Style.BRIGHT + "[-]" + Style.RESET_ALL, line)

def output_info(line):
    print(Fore.BLUE + Style.BRIGHT + "[*]" + Style.RESET_ALL, line)
